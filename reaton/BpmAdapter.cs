﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace reaton
{

    class AuthResponseStatus
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public object Exception { get; set; }
        public object PasswordChangeUrl { get; set; }
        public object RedirectUrl { get; set; }
    }


    enum OperationType
    {
        Insert,
        Select,
        Update
    }
    static class BpmAdapter
    {
        private const string baseUri = @"https://reaton.bpmonline.com";
        private const string authServiceUri = baseUri + @"/ServiceModel/AuthService.svc/Login";
        private const string selectQueryUri = baseUri + @"/0/DataService/json/reply/SelectQuery";
        private const string insertQueryUri = baseUri + @"/0/DataService/json/reply/InsertQuery";
        private const string updateQueryUri = baseUri + @"/0/DataService/json/reply/UpdateQuery";
        private static Dictionary<OperationType, string> urls = new Dictionary<OperationType, string>
        {
            {OperationType.Select, selectQueryUri },
            {OperationType.Insert, insertQueryUri },
            {OperationType.Update, updateQueryUri }
        };

        public static Tuple<bool, CookieContainer> Auth(string login, string password)
        {
            CookieContainer AuthCookie = new CookieContainer();
            var authRequest = HttpWebRequest.Create(authServiceUri) as HttpWebRequest;
            authRequest.Method = "POST";
            authRequest.ContentType = "application/json";
            authRequest.CookieContainer = AuthCookie;

            using (var requestStream = authRequest.GetRequestStream())
            {
                using (var writer = new StreamWriter(requestStream))
                {
                    writer.Write(@"{
                    ""UserName"":""" + login + @""",
                    ""UserPassword"":""" + password + @"""
                    }");
                }
            }

            AuthResponseStatus status = null;

            using (var response = (HttpWebResponse)authRequest.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string responseText = reader.ReadToEnd();
                    status = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<AuthResponseStatus>(responseText);
                }

            }

            if (status != null)
            {
                if (status.Code == 0)
                {
                    return new Tuple<bool, CookieContainer>(true, AuthCookie);
                }
                Console.WriteLine(status.Message);
            }
            return new Tuple<bool, CookieContainer>(false, AuthCookie);
        }

        public static void Send(OperationType op, string json, CookieContainer authCookie)
        {
            try
            {
                byte[] jsonArray = Encoding.UTF8.GetBytes(json);
                var uri = urls[op];
                var request = HttpWebRequest.Create(uri) as HttpWebRequest;
                request.Method = "POST";
                // Определение типа содержимого запроса.
                request.ContentType = "application/json";
                // Добавление полученных ранее аутентификационных cookie в запрос на получение данных.
                request.CookieContainer = authCookie;
                // Установить длину содержимого запроса.
                request.ContentLength = jsonArray.Length;

                // Добавление CSRF токена в заголовок запроса.
                CookieCollection cookieCollection = authCookie.GetCookies(new Uri(authServiceUri));
                string csrfToken = cookieCollection["BPMCSRF"].Value;
                request.Headers.Add("BPMCSRF", csrfToken);

                // Помещение в содержимое запроса JSON-объекта.
                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(jsonArray, 0, jsonArray.Length);
                }
                // Выполнение HTTP-запроса и получение ответа от сервера.
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    // Вывод ответа в консоль.
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        Console.WriteLine(reader.ReadToEnd());
                    }
                }
            }

            catch (WebException ex) {
                var response = ex.Response;
                if (response != null) {
                    using (var reader = new StreamReader(response.GetResponseStream())) {
                        Console.OutputEncoding = System.Text.Encoding.UTF8;
                        Console.WriteLine(reader.ReadToEnd());
                    }
                }
                throw;
            }
            catch
            {
                throw;
            }
        }
    }
}
