﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using Terrasoft.Core.Entities;
using Terrasoft.Nui.ServiceModel.DataContract;

namespace reaton
{
    interface IBPMRequestable
    {
        string Select();

        string Insert();

        string Update();

        IEnumerable<XElement> ListObjects(XDocument doc);

        IBPMRequestable FromXML(XElement el);
    }

    class Account : IBPMRequestable
    {
        private const string RootSchemaName = "Account";
        public string Id { get; set; }
        public string BpmId { get; set; }
        public string Наименование { get; set; }
        public string ВидКонтрагентаПеренос { get; set; }
        public string Код { get; set; }
        public string Страна { get; set; }
        public string Полное_Назв { get; set; }
        public string Рег_налог { get; set; }
        public string ОсновнойДоговор { get; set; }
        public string ЭлПочта { get; set; }

        public string Select()
        {
            return null;
        }
        private Dictionary<string, ColumnExpression> getColumnExpressions()
        {
            var r = new Dictionary<string, ColumnExpression>();
            if (!string.IsNullOrEmpty(Id))
            {
                r.Add("BC1CId",
                    new ColumnExpression
                    {
                        ExpressionType = EntitySchemaQueryExpressionType.Parameter,
                        Parameter = new Parameter
                        {
                            Value = Id,
                            DataValueType = DataValueType.Text
                        }
                    });
            }


            if (!string.IsNullOrEmpty(Наименование))
            {
                r.Add("Name",
                 new ColumnExpression
                 {
                     ExpressionType = EntitySchemaQueryExpressionType.Parameter,
                     Parameter = new Parameter
                     {
                         Value = Наименование,
                         DataValueType = DataValueType.Text
                     }
                 });
            }
            if (!string.IsNullOrEmpty(Код))
            {
                r.Add("Code",
                 new ColumnExpression
                 {
                     ExpressionType = EntitySchemaQueryExpressionType.Parameter,
                     Parameter = new Parameter
                     {
                         Value = Код,
                         DataValueType = DataValueType.Text
                     }
                 });
            }

            r.Add("Type",
                    new ColumnExpression
                    {
                        ExpressionType = EntitySchemaQueryExpressionType.Parameter,
                        Parameter = new Parameter
                        {
                            Value = "03a75490-53e6-df11-971b-001d60e938c6",
                            DataValueType = DataValueType.Guid
                        }
                    });
            return r;
        }

        private Filters getByIdFilter() {
            return new Filters()
            {
                FilterType = Terrasoft.Nui.ServiceModel.DataContract.FilterType.FilterGroup,
                Items = new Dictionary<string, Filter>()
                {

                {
                 "FilterById",
                 new Filter
                 {
                     FilterType = Terrasoft.Nui.ServiceModel.DataContract.FilterType.CompareFilter,
                     ComparisonType = FilterComparisonType.Equal,
                     LeftExpression = new BaseExpression()
                     {
                         ExpressionType = EntitySchemaQueryExpressionType.SchemaColumn,
                         ColumnPath = "Id"
                     },
                     RightExpression = new BaseExpression()
                     {
                         ExpressionType = EntitySchemaQueryExpressionType.Parameter,
                         Parameter = new Parameter()
                         {
                             DataValueType = DataValueType.Guid,
                             Value = BpmId
                         }
                     }
                 }
             }
         }
            };
        }

        public string Insert()
        {
            // Экземпляр класса запроса.
            var insertQuery = new InsertQuery()
            {
                RootSchemaName = RootSchemaName,
                ColumnValues = new ColumnValues()
            };
            insertQuery.ColumnValues.Items = getColumnExpressions();
            if (!string.IsNullOrEmpty(BpmId))
            {
                insertQuery.ColumnValues.Items.Add("Id",
                    new ColumnExpression
                    {
                        ExpressionType = EntitySchemaQueryExpressionType.Parameter,
                        Parameter = new Parameter
                        {
                            Value = BpmId,
                            DataValueType = DataValueType.Guid
                        }
                    });
            }


            return new JavaScriptSerializer().Serialize(insertQuery);
        }

        public string Update()
        {
            var updateQuery = new UpdateQuery()
            {
                RootSchemaName = RootSchemaName,
                ColumnValues = new ColumnValues()
                {
                    Items = getColumnExpressions()
                },
                Filters = getByIdFilter()
            };
            return new JavaScriptSerializer().Serialize(updateQuery);
        }

        public IBPMRequestable FromXML(XElement el)
        {
            Id = el.Attribute("id").Value;
            BpmId = Id.HashedGuid().ToString();
            Наименование = el.Element("Наименование")?.Value;
            ВидКонтрагентаПеренос = el.Element("ВидКонтрагентаПеренос")?.Value;
            Код = el.Element("Код")?.Value;
            Страна = el.Element("Страна")?.Value;
            Полное_Назв = el.Element("Полное_Назв")?.Value;
            Рег_налог = el.Element("Рег_налог")?.Value;
            ОсновнойДоговор = el.Element("ОсновнойДоговор")?.Value;
            ЭлПочта = el.Element("ЭлПочта")?.Value;
            return this;
        }

        public static IEnumerable<XElement> ListObjects(XDocument doc)
        {
            return (from data in doc.Descendants("Справочник")
                    where data.Attribute("Вид").Value == "Контрагенты"
                    select data.Elements()).FirstOrDefault();

        }

        IEnumerable<XElement> IBPMRequestable.ListObjects(XDocument doc)
        {
            return ListObjects(doc);
        }
    }
}
