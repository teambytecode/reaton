﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace reaton
{
    class Utils
    {
        public static void BulkInsert() {
            var xdoc = XMLParser.LoadFromFile("C:\\test.xml");
            var accounts = Account.ListObjects(xdoc);
            var authResponse = BpmAdapter.Auth("Supervisor", "Supervisor");
            if (authResponse.Item1 == false)
            {
                throw new UnauthorizedAccessException();
            }
            foreach (var a in accounts)
            {
                var account = new Account().FromXML(a);
                var q = account.Insert();
                BpmAdapter.Send(OperationType.Insert, q, authResponse.Item2);
            }
        }
    }
}
